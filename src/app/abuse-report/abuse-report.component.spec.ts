import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AbuseReportComponent } from './abuse-report.component';

describe('AbuseReportComponent', () => {
  let component: AbuseReportComponent;
  let fixture: ComponentFixture<AbuseReportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AbuseReportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AbuseReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
