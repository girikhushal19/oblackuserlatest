import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-order-confirm-combo',
  templateUrl: './order-confirm-combo.component.html',
  styleUrls: ['./order-confirm-combo.component.css']
})
export class OrderConfirmComboComponent implements OnInit {
  baseurl: any; orderid:any; userid:any
  getorderdetailbyorderid:any ;  getorderdetailbyorderidRes:any
  orderDetail:any
  clearCartApi:any
  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) {
    this.baseurl = seller.baseapiurl2
    this.orderid = this.activateroute.snapshot.params['id']
    this.userid = localStorage.getItem('main_userid')

    this.getorderdetailbyorderid = this.baseurl+"api/order/get_order_detail_by_combo_id/"+this.orderid
    this.clearCartApi = this.baseurl+"api/cart/clearcart"
  }

  ngOnInit(): void {
    this.http.get(this.getorderdetailbyorderid).subscribe(res=>{
      this.getorderdetailbyorderidRes =  res
      if(this.getorderdetailbyorderidRes.status){
        this.orderDetail = this.getorderdetailbyorderidRes.data
         const parms = {
          "userid":this.userid
        }
        this.http.post(this.clearCartApi, parms).subscribe(res=>{
          // window.location.reload()
        })
      }
    })
  }

}
