import { Component, OnInit, HostListener } from '@angular/core';
import { Route, ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { NgxImgZoomService } from 'ngx-img-zoom';
import { UserServiceService } from '../user-service.service';
import { Slick } from 'ngx-slickjs';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { VideoPlayComponent } from '../video-play/video-play.component';
import { AbuseReportComponent } from '../abuse-report/abuse-report.component';
@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {
  products: any;
  single_product: any;
  cartProductList: any;
  show = true;
  showtext = 'Voir Plus';
  ChangeImage: any
  firtsimagehide = true
  myThumbnail = ''
  myFullresImage = ''
  LoadImage: any; baseurl: any; productURL: any; myoptions: any; main_id: any
  productimages: any; cartURL: any; cartOptions: any; user_id: any; product_id: any
  product_price: any; product_qty: any; cartRes: any;
  productReviews: any
  addFavProduct: any; addFavProductRes: any
  getrecommendedproducts: any; getrecommendedproductsRes: any
  recProducts: any; recProList: any
  proCategory: any
  arrayLength = 10;
  saveUserViewProduct: any
  getUserHistoryProduct: any; getUserHistoryProductRes: any; history: any

  myvariatons: any
  variationdata: any

  productTiltle: any
  spec_attributes: any

  config: Slick.Config = {
    infinite: true,
    slidesToShow: 8,
    slidesToScroll: 2,
    dots: false,
    autoplay: true,
    autoplaySpeed: 2000,
    accessibility: false,
    responsive: [
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 6,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1008,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },

    ],
    nextArrow: "<button class='btn ' style='position: absolute !important;right: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-right' styele='font-size:5rem !important'></i></button>",
    prevArrow: "<button  class='btn '  style='position: absolute !important;left: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-left' styele='font-size:5rem !important'></i></button>",
  }

  getArray(count: number) {
    return new Array(count)
  }
  selr_id: string = ""; shop_name: string = ""; description: string = "";
  first_variation: any[] = []; second_variation: any[] = [];
  all_left_images: any[] = []; secondVariationName: string = "";
  firstVariationValue: string = ""; secondVariationValue: string = "";

  in_stock: boolean = true; total_Stock: number = 0; avgRating_record: number = 0; totalRating_record: number = 0; front_url:string="";
  apiResponse: any; variation_id: string = ""; reviews_group_record: any;
  first_variation_value_color: string = ""; first_variation_image_name: string = "";
  my_video: string = "";
  constructor(private ActiveRoute: ActivatedRoute, private http: HttpClient,
    private ngxImgZoom: NgxImgZoomService, private user: UserServiceService,private dialog: MatDialog,
     private router: Router) 
  {

    this.front_url = user.front_url;
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.proCategory = ''
    this.product_qty = 1
    this.main_id = this.ActiveRoute.snapshot.params['id']
    this.productURL = this.baseurl + "api/product/getallproducts"
    this.cartURL = this.baseurl + "api/cart/addproductocart"
    this.addFavProduct = this.baseurl + "api/product/addFavProduct"
    this.getrecommendedproducts = this.baseurl + "api/product/getrecommendedproducts"
    this.saveUserViewProduct = this.baseurl + "api/product/saveUserViewProduct"

    this.getUserHistoryProduct = this.baseurl + "api/product/getUserHistoryProduct/" + this.user_id
    // console.log("this.getUserHistoryProduct ",this.getUserHistoryProduct);


    this.myoptions = {
      catagory: "",
      singleid: this.main_id,
      sort: 1,
      pricerangeLow: "",
      pricerangeHigh: "",
      rating: "",
      condition: ""
    }
    this.variationdata = []
    this.spec_attributes = []
    this.productTiltle = '';
    
    let qq  = {user_id:this.user_id,ad_id:this.main_id};
    this.http.post(this.baseurl + "api/product/updateAdViewCount", qq).subscribe(res => {
      //this.addFavProductRes = res
      console.log("res of ad view count", res)
    })

  }

  ngOnInit(): void {

    this.findItems()
    this.getRecomended()

    const proParams = {
      "user_id": this.user_id,
      "product_id": this.main_id
    }
    this.http.post(this.saveUserViewProduct, proParams).subscribe(res => {

    })

    this.http.get(this.getUserHistoryProduct).subscribe(res => {
      this.getUserHistoryProductRes = res

      if (this.getUserHistoryProductRes.status) {
        this.history = this.getUserHistoryProductRes.date
        console.log("this.history ", this.history);

      }
    })

  }
  selectQuantity(eve: any) {

    this.product_qty = parseInt(eve.target.value)
  }
  firstVariationChange(event: any) {
    this.firstVariationValue = event.target.value;

  }
  secondVariationChange(event: any) {

    this.secondVariationValue = event.target.value;
    this.getVariationPrice();
  }
  getVariationPrice() {
    //this.findItems();
    this.product_price = this.single_product.price;

    this.total_Stock = this.single_product.stock;
    if (this.total_Stock > 0) {
      this.in_stock = true;
    } else {
      this.in_stock = false;
    }

    console.log("secondVariationValue ", this.secondVariationValue);
    console.log("firstVariationValue ", this.firstVariationValue);
    if (this.secondVariationValue != "" || this.firstVariationValue != "") {
      //
      if (this.myvariatons) {
        if (this.myvariatons.length > 0) {
          let indexxx = this.myvariatons[0].variation_option_2.indexOf(this.secondVariationValue);
          if (indexxx >= 0) {
            console.log("indexxx", indexxx);
            console.log("this.myvariatons[0].price[indexxx]", this.myvariatons[0].price[indexxx]);
            if (this.myvariatons[0].price.length >= indexxx && this.myvariatons[0].stock.length >= indexxx) {
              console.log("variation price ", this.myvariatons[0].price[indexxx]);
              console.log("variation stock ", this.myvariatons[0].stock[indexxx]);
              this.total_Stock = this.myvariatons[0].stock[indexxx];
              if (this.myvariatons[0].price[indexxx] != null && this.myvariatons[0].price[indexxx] > 0) {
                this.product_price = this.myvariatons[0].price[indexxx];
              }
              if (this.myvariatons[0].stock[indexxx] > 0) {
                this.in_stock = true;
              } else {
                this.in_stock = false;
              }


            }
          }

        }
      }
    }
  }
  findItems() {
    this.http.post(this.productURL, this.myoptions).subscribe(res => {
      this.products = res
      this.productimages = this.products.data
      this.single_product = this.productimages[0];

      this.all_left_images = this.productimages[0].images;

      this.selr_id = this.single_product.selr_id;
      this.shop_name = this.single_product.shop_name;
      this.description = this.single_product.description;

      //console.log("single_product ", this.single_product);
      //return false;
      if (this.single_product.video) {
        if (this.single_product.video != "") {
          //console.log("this.single_product.video ", this.single_product.video);
          this.my_video = this.single_product.video;
        }
      }
      this.reviews_group_record = this.single_product.reviews_group;
      this.avgRating_record = this.single_product.avgRating;
      //console.log("reviews_group_record ", this.reviews_group_record);
      //reviews_group
      this.productTiltle = this.single_product.title
      this.productReviews = this.single_product.reviews;
      this.totalRating_record = this.single_product.reviews.length;
      if (this.single_product.images.length > 0) {
        this.LoadImage = this.baseurl + 'static/public' + this.single_product.images[0];
      } else {
        this.LoadImage = this.baseurl + 'static/public/media/no-image.jpeg';
      }


      this.product_id = this.single_product._id
      this.product_price = this.single_product.price;

      this.total_Stock = this.single_product.stock;
      if (this.total_Stock > 0) {
        this.in_stock = true;
      } else {
        this.in_stock = false;
      }

      this.proCategory = this.single_product.catagory
      this.myvariatons = this.productimages[0].variations;
      console.log("myvariatons ", this.myvariatons);

      if (this.myvariatons) {
        if (this.myvariatons.length > 0) {
          this.secondVariationName = this.myvariatons[0].variation_2;
          this.variation_id = this.myvariatons[0]._id;
          if (this.myvariatons[0].images.length > 0) {
            if (this.myvariatons[0].images[0].length > 0) {
              this.first_variation_image_name = this.myvariatons[0].images[0][0];
            }
          }
          //
          let indexxx = 0;
          // if(this.myvariatons[0].price[indexxx] != null && this.myvariatons[0].price[indexxx] > 0 )
          //   {
          //     this.product_price = this.myvariatons[0].price[indexxx];
          //   }
          //   if(this.myvariatons[0].stock[indexxx] > 0)
          //   {
          //     this.in_stock = true;
          //   }else{
          //     this.in_stock = false;
          //   }
        }
      }

      if (this.productimages[0].variations) {
        if (this.productimages[0].variations.length > 0) {
          this.variationdata = this.productimages[0].sales_info_variation_prices[0];
        }
      }

      this.spec_attributes = this.single_product.spec_attributes
      this.first_variation = this.products.vari;
      if (this.first_variation) {
        if (this.first_variation.length > 0) {
          if (this.first_variation[0]._id.length > 0) {
            this.first_variation_value_color = this.first_variation[0]._id[0];
            this.firstVariationValue = this.first_variation[0]._id[0];
            //console.log("this.first_variation_value_color ",this.first_variation_value_color);
          }
        }
      }
      this.second_variation = this.products.vari_2;
      console.log("second_variation ", this.second_variation);
      console.log("second_variation ", this.second_variation.length);

      const params = {
        "catagory": this.proCategory,
        "id": this.single_product.category_id[0]
      }

      this.http.post(this.getrecommendedproducts, params).subscribe(res => {
        this.getrecommendedproductsRes = res


        if (this.getrecommendedproductsRes.status) {
          this.recProducts = this.getrecommendedproductsRes
          this.recProList = this.recProducts.data
          console.log("this.recProList ", this.recProList);

        }
      })
      // console.log("this.LoadImage ",this.LoadImage);
      this.myLoadFunff(this.LoadImage);
      // console.log("first_variation_image_name ", this.first_variation_image_name);


    })
  }
  //@HostListener('window:load')
  myLoadFunff(pic: any) {
    console.log("load function");
    // console.log(pic);
    this.ChangeImage = pic;
  }
  onSetIndex(pic: any) {

    this.ChangeImage = pic.target.src
    console.log(this.ChangeImage);
    this.firtsimagehide = true
  }

  onSetIndex2(pic: any, item: any, colr_name: any) {
    this.variationdata = item
    //console.log(item)
    this.ChangeImage = pic.target.src
    this.firtsimagehide = true;
    //console.log("colr_name ",colr_name);
    this.first_variation_value_color = colr_name;
  }
  rightSideMainImageClick(pic: any, item: any, colr_name: any, indexxx: number = 0) {
    this.variationdata = item
    console.log(item)
    this.ChangeImage = pic.target.src
    this.firtsimagehide = true;
    //  console.log("colr_name ",colr_name);
    this.first_variation_value_color = colr_name;
    this.first_variation_image_name = item;
    this.firstVariationValue = colr_name;

    //console.log("indexxx ",indexxx);
    if (this.myvariatons) {
      if (this.myvariatons.length > 0) {
        if (this.myvariatons[0].images.length >= indexxx) {
          this.all_left_images = [];
          //console.log("this.myvariatons 0  ", this.myvariatons[0].images[indexxx]);
          //this.LoadImage = this.myvariatons[0].images[indexxx];
          this.myvariatons[0].images[indexxx].forEach((val: any) => {
            this.all_left_images.push("/media/" + val);
          });
          //console.log("this.all_left_images ",this.all_left_images);
        }
      }

    }
    //console.log("first_variation_image_name ", this.first_variation_image_name);
    this.getVariationPrice();
  }
  numSequence(n: number): Array<number> {
    return Array(n);
  }
  addCart(cartItem: any, val: any) {
    console.log("heeeeeeeeeeeeeeeeeeeeeeeeeee");
    console.log("heeeeeeeeeeeeeeeeeeeeeeeeeee",this.user_id);

    //this.first_variation_image_name = 
    // console.log("secondVariationValue ",this.secondVariationValue);
    // console.log("firstVariationValue ",this.firstVariationValue);
    
    if(this.user_id == "" || this.user_id == null || this.user_id == undefined || this.user_id == 'null' || this.user_id == 'undefined')
    {
      //window.location.href = "/login";
      this.router.navigate(['login'])
    }else{
      this.cartOptions = {
        userid: this.user_id,
        productid: this.product_id,
        first_variation_image_name: this.first_variation_image_name,
        firstVariationValue: this.firstVariationValue,
        secondVariationName: this.secondVariationName,
        secondVariationValue: this.secondVariationValue,
        variation_id: this.variation_id,
        variations: [],
        price: this.product_price,
        //quantity:1
        quantity: this.product_qty
      }
  
      this.http.post(this.cartURL, this.cartOptions).subscribe(res => {
        this.cartRes = res
  
        if (this.cartRes.status == true) {
          // alert("Item added to Cart successfully")
          if (val == 0) {
            window.location.href = this.front_url+"cart"
          }
          else if (val == 1) {
            window.location.href = this.front_url+"checkout"
          }
  
        } else {
  
        }
  
      })
    }
    
    
    

  }
  showMore() {
    // console.log(this.show)
    if (this.show == true) {
      this.show = false;
      this.showtext = 'Voir moins'
    } else {
      this.show = true;
      this.showtext = 'Voir plus'
    }

  }
  addFavItem() {

    const parms = {
      "product_id": this.main_id,
      "user_id": this.user_id
    }

    this.http.post(this.addFavProduct, parms).subscribe(res => {
      this.addFavProductRes = res

      if (this.addFavProductRes.status) {
        window.location.href = "/wish_list"
      }
    })

  }

  getRecomended() {

  }
  openvideoDialog(video:any){
    this.dialog.open(VideoPlayComponent,{
      data: { 
        "video" : video
      }
    });
  }


  openAbuseReport(){
    
    this.dialog.open( AbuseReportComponent ,{
      data: { 
        "ad_id" : this.main_id
      }
    });

  }


}


