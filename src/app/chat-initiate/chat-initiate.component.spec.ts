import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChatInitiateComponent } from './chat-initiate.component';

describe('ChatInitiateComponent', () => {
  let component: ChatInitiateComponent;
  let fixture: ComponentFixture<ChatInitiateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChatInitiateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChatInitiateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
