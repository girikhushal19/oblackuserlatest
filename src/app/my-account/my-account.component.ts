import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {
  baseurl:any; mainid:any; apiResponse:any; front_url:any;
  last_login_time:any;
  constructor(private userService:UserServiceService)
  {
    this.mainid = localStorage.getItem('main_userid')
    this.baseurl = userService.baseapiurl2;
    this.front_url = userService.front_url;
    
    this.last_login_time = localStorage.getItem('last_login_time');


    console.log(" this.mainid " , this.mainid);
    if(this.mainid == "" || this.mainid == null || this.mainid == undefined ||  this.mainid == 'null' || this.mainid == 'undefined')
    {
      window.location.href = userService.front_url+"login";
    }

    console.log(" this.last_login_time " , this.last_login_time);
    if(this.last_login_time == "" || this.last_login_time == null || this.last_login_time == undefined ||  this.last_login_time == 'null' || this.last_login_time == 'undefined')
    {
      window.location.href = userService.front_url+"login";
    }
    // else{
    //   const date = new Date();
    //   //console.log("this.base_url"+this.base_url);
    //   var hours = Math.abs(date.getTime() - new Date(this.last_login_time).getTime()) / 3600000;
    //   console.log("hours "+hours);
    //   if(hours > 24)
    //   {
    //     localStorage.clear();
    //     window.location.href = userService.front_url+"login";
    //   }
    // }
  }

  ngOnInit(): void {
  }

}
