import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MyExpenditionComponent } from './my-expendition.component';

describe('MyExpenditionComponent', () => {
  let component: MyExpenditionComponent;
  let fixture: ComponentFixture<MyExpenditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MyExpenditionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MyExpenditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
