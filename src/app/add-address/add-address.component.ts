import { HttpClient } from '@angular/common/http';
import { Component, ViewChild, EventEmitter, Output, OnInit, AfterViewInit, Input,ElementRef } from '@angular/core';
import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
//import { Component, OnInit, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
 

@Component({
  selector: 'app-add-address',
  templateUrl: './add-address.component.html',
  styleUrls: ['./add-address.component.css']
})
export class AddAddressComponent implements OnInit {
  CheckoutProducts: any; addressform!: FormGroup
  myAddressForm: any; user_email: any; baseurl: any; addressURL: any
  fulldata: any; getAddressURL: any; user_id: any
  myaddressArray: any; Accesscode: any; Intephone: any; instructions: any
  latitude:number=0;
  longitude:number=0;
  endAddress: any;apiResponse:any;
  myCountryCode:any;   googleAddress:any;
  my_country: any; my_city: any; my_po:any; countries:any;
  nrSelect = "+225"
  options: Options = new Options({ 
    componentRestrictions: {country: 'ci'}
  }); 
  constructor(private user: UserServiceService, private http: HttpClient,
    private router: Router) {

    this.baseurl = user.baseapiurl2
    this.user_email = localStorage.getItem("useremail")
    this.user_id = localStorage.getItem("main_userid")
    this.addressURL = this.baseurl + "api/user/updateAddressDetail"
    this.getAddressURL = this.baseurl + "api/user/getuserprofile/"
    this.CheckoutProducts = []
    this.myaddressArray = []
    this.my_country = ''
    this.my_city = ''
    this.my_po = ''
    this.myAddressForm = {
      email: this.user_email
    }
    this.addressform = new FormGroup({
      name: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      pobox: new FormControl(this.my_po ),
      country: new FormControl(this.my_country),
      city: new FormControl(this.my_city),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      countryCode: new FormControl(''),
    })
    this.Accesscode = ''
    this.Intephone = ''
    this.instructions = '';


 


    this.countries = [
      {
        "code": "+225",
        "name": "Ivory Coast"
      },
      {
        "code": "+33",
        "name": "Ivory Coast"
      }
    ]
    this.myCountryCode = '+33'
  }
  google: any;
  ngOnInit(): void {
     
  }
  
 
  addAddress() {
    console.log("adderss submit 54");
    const delivery_instructions = {
      accesscode: this.Accesscode.value,
      interphone: this.Intephone.value,
      requiredcode: '',
      instructions: this.instructions.value
    }
    this.addressform.value.delivery_instructions = delivery_instructions
    

    let addressss =  {
      address: this.endAddress,
      latlong: [ this.latitude,this.longitude ]
    }


    this.myAddressForm.address = this.addressform.value
    this.myAddressForm.addresss = addressss
    this.myAddressForm.address.address = this.endAddress
    this.myAddressForm.address.city = this.my_city
    this.myAddressForm.address.country = this.my_country
    if(this.my_po)
    {
      this.myAddressForm.address.pobox = this.my_po;
    }else{
      this.myAddressForm.address.pobox = this.addressform.value.pobox;
    }
    
     
    
    //this.longitude = parseFloat(this.longitude); 

    this.myAddressForm.address.Location = {
      type:"Point",
      coordinates:[this.latitude,this.longitude]
    }
    //endLocation: { type: 'Point', coordinates: [ 48.8676635, 2.3640304 ] }
    // this.myAddressForm.address.latitude = this.latitude;
    // this.myAddressForm.address.longitude = this.longitude;

    console.log("myAddressForm ",this.myAddressForm)
    if (this.addressform.valid)
    {
      console.log("form valid 71");
      if(this.latitude == 0 || this.latitude == null || this.latitude == undefined || this.longitude == 0 || this.longitude == null || this.longitude == undefined)
      {
        this.apiResponse = {status:false,errorMessage:"L'adresse ne doit être que l'adresse de Google"};
        return;
      }else{
        this.http.post(this.addressURL, this.myAddressForm).subscribe(res => {
          this.fulldata = res;
          this.apiResponse = res;
          console.log(this.fulldata)
          console.log("this.apiResponse  ",this.apiResponse )
          if (this.fulldata.status == true) {
            //window.location.reload()
            setTimeout(()=>{
              window.location.reload()
            },2000);
            
          }
        })
      }
      //return false;
      
    }
    else{
      console.log("form in valid 84");
      for (const control of Object.keys(this.addressform.controls)) {
        this.addressform.controls[control].markAsTouched();
      }
      return;
    }




  }

  handleAddressChange2(address2: any) {
    this.my_po = '';
    console.log(address2.address_components);



    this.latitude = address2.geometry.location.lat()
    this.longitude = address2.geometry.location.lng()
    console.log("this.latitude ", this.latitude);
    console.log("this.longitude ", this.longitude);

    var country = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("country"))
     var po_box = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("postal_code"))
    var city = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("locality"))
    ///console.log(country[0].long_name, city[0].long_name)
    if(country)
    {
      if(country.length > 0)
      { 
        this.my_country = country[0].long_name;
        
        
      }
      if(city.length > 0)
      {
        this.my_city = city[0].long_name;
      }
      if(po_box.length > 0)
      {
        this.my_po = po_box[0].long_name;
      }
    }
   

    this.endAddress = address2.formatted_address
    // this.endLatitude = address2.geometry.location.lat()
    // this.endLongitude = address2.geometry.location.lng()
    // this.mypickaddress.address = this.endAddress
    // this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]


  }

  selectCounrtyCode(eve:any){
   
    this.myCountryCode = eve.target.value
  }
}
