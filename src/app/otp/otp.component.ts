import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {
  baseurl: any
  status: any
  registerform !: FormGroup
  mainid: any
  resendotp: any; otpoptions: any
  myemail: any

  constructor(private seller: UserServiceService, private http: HttpClient,
    private router: Router, private activateroute: ActivatedRoute) {
    this.baseurl = seller.baseapiurl2
    this.myemail = localStorage.getItem('useremail')
    this.resendotp = this.baseurl + "api/user/resendOtp",
    this.mainid = this.activateroute.snapshot.params['id']
    this.registerform = new FormGroup({

      code: new FormControl(''),

    })
  }

  ngOnInit(): void {

  }
  verify() {
    this.registerform.value.id = this.mainid
    console.log(this.registerform.value)
    this.http.post(this.baseurl + "api/user/verifycode", this.registerform.value).subscribe(res => {

      this.status = res
      console.log(this.status.status)
      if (this.status.status == true) {
        this.router.navigate(['login'])
      }
      else {

      }
    })
  }
  resendOTP() {

    const params ={
      "email":this.myemail
    }

    this.http.post( this.resendotp, params).subscribe(res=>{
     
      this.status = res
      console.log(this.status)
    })
  }

}
