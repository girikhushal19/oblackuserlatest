import { HttpClient } from '@angular/common/http';
import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserServiceService } from '../user-service.service';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Options } from 'ngx-google-places-autocomplete/objects/options/options';
@Component({
  selector: 'app-edit-address',
  templateUrl: './edit-address.component.html',
  styleUrls: ['./edit-address.component.css']
})
export class EditAddressComponent implements OnInit {
  CheckoutProducts: any; addressform!: FormGroup
  myAddressForm: any; user_email: any; baseurl: any; addressURL: any
  fulldata: any; getAddressURL: any; user_id: any
  myaddressArray: any; Accesscode: any; Intephone: any; instructions: any
  latitude:number=0;
  longitude:number=0;
  endAddress: any;apiResponse:any;api_response:any;
  myCountryCode:any
  my_country: any; my_city: any; my_po:any; countries:any;
  nrSelect = "+225";
  eee:any;   googleAddress:any;
  old_rec :any;
  old_name :any; old_country :any; old_countryCode :any; old_telephone :any; old_address :any;  
      addressss:any;
        old_pobox:any; old_city:any; old_accesscode:any;    old_interphone:any;    old_instructions:any;address_idd:any;

        options: Options = new Options({ 
          componentRestrictions: {country: 'ci'}
        }); 


  constructor(private user: UserServiceService, private http: HttpClient,
    private router: Router,@Inject(MAT_DIALOG_DATA) public data: any) {

      this.Accesscode = ''
      this.Intephone = ''
      this.instructions = '';

    this.baseurl = user.baseapiurl2
    this.user_email = localStorage.getItem("useremail")
    this.user_id = localStorage.getItem("main_userid")
    this.addressURL = this.baseurl + "api/user/updateAddressDetail"
    this.getAddressURL = this.baseurl + "api/user/getuserprofile/"
   
    this.CheckoutProducts = []
    this.myaddressArray = []
    this.my_country = ''
    this.my_city = ''
    this.my_po = ''
    this.myAddressForm = {
      email: this.user_email
    }

    this.http.post(this.baseurl + "api/user/getUserSingleAddress/", {id:this.data.address_id,user_id:this.user_id}).subscribe(res => {
       
      this.api_response = res; 
      console.log("this.apiResponse  ",this.api_response )
      this.old_rec = this.api_response.data;
      console.log("this.old_rec ", this.old_rec);
      console.log("this.old_rec ", this.old_rec.name);
      this.old_name = this.old_rec.name;
      this.my_country = this.old_rec.country;
      this.old_countryCode = this.old_rec.countryCode;
      this.old_telephone = this.old_rec.telephone;
      this.old_address = this.old_rec.address.address;
      this.googleAddress = this.old_rec.address.address;
      this.addressss = this.old_rec.address;
      this.my_po = this.old_rec.pobox;
      this.my_city = this.old_rec.city;
      //this.old_city = this.old_rec.address.city;

      console.log("this.old_rec.delivery_instructions ", this.old_rec.delivery_instructions);
      
      if(this.old_rec.delivery_instructions)
      {
        console.log("hereeeee 69");
        this.old_accesscode = this.old_rec.delivery_instructions.accesscode;
        this.old_interphone = this.old_rec.delivery_instructions.interphone;
        this.old_instructions = this.old_rec.delivery_instructions.instructions;
        
      }
     
     


      if(this.old_accesscode == undefined || this.old_accesscode == 'undefined')
      {
        console.log("this.old_accesscode ", this.old_accesscode)
        this.old_accesscode = "";
      }

      if(this.old_interphone == undefined || this.old_interphone == 'undefined')
      {
        this.old_interphone = "";
      }

      if(this.old_instructions == undefined || this.old_instructions == 'undefined')
      {
        this.old_instructions = "";
      }


      if(this.old_rec.address)
      {
        if(this.old_rec.address.latlong)
        {
          if(this.old_rec.address.latlong.length > 1)
          {
            this.longitude = this.old_rec.address.latlong[1];
            this.latitude = this.old_rec.address.latlong[0];
          }
        }
      }
    

    })


    this.addressform = new FormGroup({
      name: new FormControl('', Validators.required),
      telephone: new FormControl('', Validators.required),
      address: new FormControl('', Validators.required),
      pobox: new FormControl(this.my_po ),
      country: new FormControl(this.my_country),
      city: new FormControl(this.my_city),
      latitude: new FormControl(''),
      longitude: new FormControl(''),
      countryCode: new FormControl(''),
      address_id: new FormControl(''),
      access_code: new FormControl(''),
      interphone: new FormControl(''),
      instruction: new FormControl(''),
    })
   


    this.countries = [
      {
        "code": "+225",
        "name": "Ivory Coast"
      },
      {
        "code": "+33",
        "name": "Ivory Coast"
      }
    ]
    this.myCountryCode = '+33';

     //
     
  }

  ngOnInit(): void {
    console.log(this.data);
    this.address_idd = this.data.address_id;
  }

  editAddress() {
    console.log("adderss submit 54");
    const delivery_instructions = {
      accesscode: this.addressform.value.access_code,
      interphone: this.addressform.value.interphone,
      requiredcode: '',
      instructions: this.addressform.value.instruction,
    }
    this.addressform.value.delivery_instructions = delivery_instructions


    let addressss =  {
      address: this.googleAddress,
      latlong: [ this.latitude,this.longitude ]
    }


    this.myAddressForm.address = this.addressform.value
    this.myAddressForm.addresss = addressss
    this.myAddressForm.address.address = this.endAddress
    this.myAddressForm.address.city = this.my_city
    this.myAddressForm.address.country = this.my_country
    //this.myAddressForm.address.pobox = this.my_po;
    if(this.my_po)
    {
      this.myAddressForm.address.pobox = this.my_po;
    }else{
      this.myAddressForm.address.pobox = this.addressform.value.pobox;
    }

    this.myAddressForm.address_id = this.address_idd;
     
    
    //this.longitude = parseFloat(this.longitude); 

    this.myAddressForm.address.Location = {
      type:"Point",
      coordinates:[this.latitude,this.longitude]
    }
    //endLocation: { type: 'Point', coordinates: [ 48.8676635, 2.3640304 ] }
    // this.myAddressForm.address.latitude = this.latitude;
    // this.myAddressForm.address.longitude = this.longitude;

    console.log("myAddressForm ",this.myAddressForm)
    if (this.addressform.valid)
    {
      console.log("form valid 71");
      if(this.latitude == 0 || this.latitude == null || this.latitude == undefined || this.longitude == 0 || this.longitude == null || this.longitude == undefined)
      {
        this.apiResponse = {status:false,errorMessage:"L'adresse ne doit être que l'adresse de Google"};
        return;
      }else{
        this.http.post(this.addressURL, this.myAddressForm).subscribe(res => {
          this.fulldata = res;
          this.apiResponse = res;
          console.log(this.fulldata)
          console.log("this.apiResponse  ",this.apiResponse )
          if (this.fulldata.status == true) {
            //window.location.reload()
            setTimeout(()=>{
              window.location.reload()
            },2000);
            
          }
        })
      }
      //return false;
      
    }
    else{
      console.log("form in valid 84");
      for (const control of Object.keys(this.addressform.controls)) {
        this.addressform.controls[control].markAsTouched();
      }
      return;
    }




  }

  handleAddressChange2(address2: any) {
    console.log(address2.address_components);
    console.log("address2 " , address2.formatted_address);
    console.log("address2 " , address2);
    this.googleAddress = address2.formatted_address;


    this.latitude = address2.geometry.location.lat()
    this.longitude = address2.geometry.location.lng()

    var country = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("country"))
     var po_box = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("postal_code"))
    var city = address2.address_components.filter((component: { types: string | string[]; }) => component.types.includes("locality"))
    ///console.log(country[0].long_name, city[0].long_name)
    if(country)
    {
      if(country.length > 0)
      { 
        this.my_country = country[0].long_name;
        
        
      }
      if(city.length > 0)
      {
        this.my_city = city[0].long_name;
      }
      if(po_box.length > 0)
      {
        this.my_po = po_box[0].long_name;
      }
    }
   

    this.endAddress = address2.formatted_address
    // this.endLatitude = address2.geometry.location.lat()
    // this.endLongitude = address2.geometry.location.lng()
    // this.mypickaddress.address = this.endAddress
    // this.mypickaddress.latlong = [this.endLatitude, this.endLongitude]


  }

  selectCounrtyCode(eve:any){
   
    this.myCountryCode = eve.target.value
  }
}
