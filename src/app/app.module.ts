import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { LivresComponent } from './livres/livres.component';
import { MusiqueEtDvdComponent } from './musique-et-dvd/musique-et-dvd.component';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { JeuxVideoEtConsoiesComponent } from './jeux-video-et-consoies/jeux-video-et-consoies.component';
import { TablettesComponent } from './tablettes/tablettes.component';
import { EvenementsComponent } from './evenements/evenements.component';
import { FooterComponent } from './footer/footer.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { SigninComponent } from './signin/signin.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EventsModule } from './events/events.module';
import { FormsModule } from '@angular/forms';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MyAccountComponent } from './my-account/my-account.component';
import { CartComponent } from './cart/cart.component';
import { CurrencyPipe } from '@angular/common';
import { CategoryComponent } from './category/category.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { ViewProductComponent } from './view-product/view-product.component';
import { NgxImgZoomModule } from 'ngx-img-zoom';
import { LoginComponent } from './login/login.component';
import { NgxImageZoomModule } from 'ngx-image-zoom';
import { CheckoutComponent } from './checkout/checkout.component';
import { SellerdashboardComponent } from './sellerdashboard/sellerdashboard.component';
import { ProductOrderComponent } from './product-order/product-order.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { MyExpenditionComponent } from './my-expendition/my-expendition.component';
import { SelleraddproductComponent } from './selleraddproduct/selleraddproduct.component';
import { AdminpanelComponent } from './adminpanel/adminpanel.component';
import { AdmindashboardComponent } from './admindashboard/admindashboard.component';
import { SellerhomeComponent } from './sellerhome/sellerhome.component';
import { ShipmentComponent } from './shipment/shipment.component';
import { OrdershipComponent } from './ordership/ordership.component';
import { MyproductsComponent } from './myproducts/myproducts.component';
import { MyordersComponent } from './myorders/myorders.component';
import { SellerregisterComponent } from './sellerregister/sellerregister.component';
import { MatStepperModule } from '@angular/material/stepper';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from "@angular/material/form-field";
import { TestComponent } from './test/test.component';
import { OtpComponent } from './otp/otp.component';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
import { FilterProductsComponent } from './filter-products/filter-products.component';
import { AddAddressComponent } from './add-address/add-address.component';
import { TermsandconditionsComponent } from './termsandconditions/termsandconditions.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { MyaddressComponent } from './myaddress/myaddress.component';
import { SecurityComponent } from './security/security.component';
import { PromocodeComponent } from './promocode/promocode.component';
import { EventFavComponent } from './event-fav/event-fav.component';
import { MyEventsComponent } from './my-events/my-events.component';
import { CategoryFilterComponent } from './category-filter/category-filter.component';
import {  NgxSlickJsModule } from 'ngx-slickjs';
import { OrderConfirmationComponent } from './order-confirmation/order-confirmation.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { MessegesChatComponent } from './messeges-chat/messeges-chat.component';
import { TarackingComponent } from './taracking/taracking.component';
import { EventBootcampComponent } from './event-bootcamp/event-bootcamp.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { SellerProfileComponent } from './seller-profile/seller-profile.component';
import { WishListComponent } from './wish-list/wish-list.component';
import { ReturnOrderComponent } from './return-order/return-order.component';
import { RateSellerComponent } from './rate-seller/rate-seller.component';
import { ProductReviewComponent } from './product-review/product-review.component';
import { CancelOrderComponent } from './cancel-order/cancel-order.component';
import { ViewOrderComponent } from './view-order/view-order.component';
import { EventHomeComponent } from './event-home/event-home.component';
import { EventTicketsComponent } from './event-tickets/event-tickets.component';
import { EventLikeComponent } from './event-like/event-like.component';
import { EventCartComponent } from './event-cart/event-cart.component';
import { EventConfirmComponent } from './event-confirm/event-confirm.component';
import { AddNameComponent } from './add-name/add-name.component';
import { AddPhoneComponent } from './add-phone/add-phone.component';
import { ChangePasswordComponent } from './change-password/change-password.component'
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ChatInitiateComponent } from './chat-initiate/chat-initiate.component';
import { GoogleMapsModule } from '@angular/google-maps';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');
import { LOCALE_ID } from '@angular/core';
import { VideoPlayComponent } from './video-play/video-play.component';
import {MAT_MOMENT_DATE_FORMATS, MomentDateAdapter} from '@angular/material-moment-adapter';
import {DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { AbuseReportComponent } from './abuse-report/abuse-report.component';
import { EditAddressComponent } from './edit-address/edit-address.component';
import { OrderConfirmComboComponent } from './order-confirm-combo/order-confirm-combo.component';
import {CookieService} from 'ngx-cookie-service';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LivresComponent,
    MusiqueEtDvdComponent,
    JeuxVideoEtConsoiesComponent,
    TablettesComponent,
    EvenementsComponent,
    FooterComponent,
    RegisterComponent,
    HomeComponent,
    SigninComponent,
    MyAccountComponent,
    CartComponent,
    CategoryComponent,
    ViewProductComponent,
    LoginComponent,
    CheckoutComponent,
    SellerdashboardComponent,
    ProductOrderComponent,
    EventDetailComponent,
    MyExpenditionComponent,
    SelleraddproductComponent,
    AdminpanelComponent,
    AdmindashboardComponent,
    SellerhomeComponent,
    ShipmentComponent,
    OrdershipComponent,
    MyproductsComponent,
    MyordersComponent,
    SellerregisterComponent,
    TestComponent,
    OtpComponent,
    FilterProductsComponent,
    AddAddressComponent,
    TermsandconditionsComponent,
    ResetPasswordComponent,
    MyaddressComponent,
    SecurityComponent,
    PromocodeComponent,
    EventFavComponent,
    MyEventsComponent,
    CategoryFilterComponent,
    OrderConfirmationComponent,
    ForgetPasswordComponent,
    MessegesChatComponent,
    TarackingComponent,
    EventBootcampComponent,
    AboutUsComponent,
    ContactUsComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent,
    SellerProfileComponent,
    WishListComponent,
    ReturnOrderComponent,
    RateSellerComponent,
    ProductReviewComponent,
    CancelOrderComponent,
    ViewOrderComponent,
    EventHomeComponent,
    EventTicketsComponent,
    EventLikeComponent,
    EventCartComponent,
    EventConfirmComponent,
    AddNameComponent,
    AddPhoneComponent,
    ChangePasswordComponent,
    ChatInitiateComponent,
    VideoPlayComponent,
    AbuseReportComponent,
    EditAddressComponent,
    OrderConfirmComboComponent,

  ],
  imports: [
    GooglePlaceModule,
    BrowserModule,
    AppRoutingModule,
    MatDialogModule,
    BrowserAnimationsModule,
    MatButtonModule,
    ReactiveFormsModule,
    EventsModule,
    FormsModule,
    Ng2SearchPipeModule,
    CommonModule,
    MatPaginatorModule,
    HttpClientModule,
    NgxPaginationModule,
    NgxImgZoomModule,
    NgxImageZoomModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    AutocompleteLibModule,
    NgbModule,
    MatInputModule,
    MatDatepickerModule,
    GoogleMapsModule,
    NgxSlickJsModule.forRoot({
      links: {
        slickJs: "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js",
        slickCss: "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css",
        slickThemeCss: "https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"
      }
  })

    
  ],
  providers: [CurrencyPipe,
    { provide: LOCALE_ID, useValue: "fr-FR" },
    {provide: MAT_DATE_LOCALE, useValue: 'fr-FR'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},CookieService],
    
  bootstrap: [AppComponent]
})
export class AppModule { }
