import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from 'src/app/user-service.service';
@Component({
  selector: 'app-event-tickets',
  templateUrl: './event-tickets.component.html',
  styleUrls: ['./event-tickets.component.css']
})
export class EventTicketsComponent implements OnInit {
  baseurl:any; user_id:any
  getongoingeventsbyuserid:any; onApires:any; onRecords:any
  constructor(private http: HttpClient, private user: UserServiceService) { 
    this.user_id = localStorage.getItem('main_userid')
    this.baseurl = user.baseapiurl2
    this.getongoingeventsbyuserid = this.baseurl + "api/event/getongoingeventsbyuserid/"+this.user_id
  }

  ngOnInit(): void {
    this.http.get(this.getongoingeventsbyuserid).subscribe(res=>{
      this.onApires = res
      if(this.onApires.status){
        this.onRecords = this.onApires.data
      }
    })
  }

}
