import { Component, HostListener, OnInit } from '@angular/core';
import { ViewportScroller } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { UserServiceService } from '../user-service.service';
import { Slick } from 'ngx-slickjs';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  baseurl: any; bannerURL: any; bannerData: any
  topCategoriesURL: any; topCategoriesData: any
  feauturedCategoriesURL: any; feauturedCategoriesData: any
  firstBannerImg: any
  addFavProduct: any; addFavProductRes: any
  homepro: any
  user_id: any
  deleteFavProduct: any; deleteFavProductRes: any
  getMiddlebannerimages: any; smallBanners: any
  middlebanner: any;
  showBtn:number= -1; msGG:string=""; showBtn2:number= 0;
  constructor(private viewportScroller: ViewportScroller, private http: HttpClient, private user: UserServiceService,) {
    this.baseurl = user.baseapiurl2
    this.user_id = localStorage.getItem('main_userid')
    this.bannerURL = this.baseurl + "api/admin/getallbannerimage"
    this.topCategoriesURL = this.baseurl + "api/admin/gettopcatagories"
    this.feauturedCategoriesURL = this.baseurl + "api/admin/getFeatruedProductcatagory"
    this.addFavProduct = this.baseurl + "api/product/addFavProduct"
    this.deleteFavProduct = this.baseurl + "api/product/deleteFavProduct/"
    this.getMiddlebannerimages = this.baseurl + "api/admin/getMiddlebannerimages"
    this.homepro = []
  }

  arrayLength = 10;

  config: Slick.Config = {
    infinite: true,
    slidesToShow: 6,
    slidesToScroll: 2,
    dots: false,
    autoplay: false,
    autoplaySpeed: 2000,
    accessibility: false,
    responsive: [
      
      {
        breakpoint: 1200,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 1008,
        settings: {
          slidesToShow: 5,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 900,
        settings: {
          slidesToShow: 4,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },
      {
        breakpoint: 400,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
        },
      },

    ],
    nextArrow: "<button class='btn ' style='position: absolute !important;right: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-right' styele='font-size:5rem !important'></i></button>",
    prevArrow: "<button  class='btn '  style='position: absolute !important;left: 0 !important;top: 50% !important;z-index: 2 !important;background: #eaeded; color: rgba(0, 0, 0, 0.4);height: 75px; border: none !important; width: 50px;margin: 0;border-radius: 0;outline: none !important;box-shadow: none !important;'><i class='fa fa-angle-left' styele='font-size:5rem !important'></i></button>",
  }

  getArray(count: number) {
    return new Array(count)
  }

  ngOnInit(): void {
    this.getFeaturedCategories()
    this.getMiddleBanner()
  }

  getFeaturedCategories() {
    const params = {
      "user_id": this.user_id
    }
    this.http.post(this.feauturedCategoriesURL, params).subscribe(res => {
      this.feauturedCategoriesData = res

      if (this.feauturedCategoriesData.status) {
        this.homepro = this.feauturedCategoriesData.data;
        console.log("this.feauturedCategoriesURL ", this.feauturedCategoriesURL);
        console.log("homepro ",this.homepro)
      }
      


    })
  }
  addFavItem(productid: any,indexOne:number,indexTwo:number) {


    this.showBtn=indexOne;   this.showBtn2 = indexTwo;


    const parms = {
      "product_id": productid,
      "user_id": this.user_id
    }

    this.http.post(this.addFavProduct, parms).subscribe(res => {
      this.addFavProductRes = res

      // if (this.addFavProductRes.status) {
      //   window.location.reload()
      // }

      this.msGG = this.addFavProductRes.message;
        setTimeout(()=>{
          window.location.reload()
        },2000)


    })

  }
  removeFavItem(productid: any,indexOne:number,indexTwo:number) {
    this.showBtn=indexOne;   
    this.showBtn2 = indexTwo;
    this.http.get(this.deleteFavProduct + productid).subscribe(res => {
      this.deleteFavProductRes = res
      console.log(res)

      // if (this.deleteFavProductRes.status) {
      //   window.location.reload()
      // }

      this.msGG = this.deleteFavProductRes.message;
        setTimeout(()=>{
          window.location.reload()
        },2000)


    })
  }

  getMiddleBanner() {
    this.http.get(this.getMiddlebannerimages).subscribe(res => {
      this.smallBanners = res
      this.middlebanner = this.smallBanners.data

      console.log(this.middlebanner, "dfhdfsudjheujh")

    })
  }



}
