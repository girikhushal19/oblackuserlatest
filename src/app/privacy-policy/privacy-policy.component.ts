import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../user-service.service';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-privacy-policy',
  templateUrl: './privacy-policy.component.html',
  styleUrls: ['./privacy-policy.component.css']
})
export class PrivacyPolicyComponent implements OnInit {
  baseurl:any
  user_web_privacy_policy:any; user_web_privacy_policyRes:any
  myRec:any; termsPage:any
  constructor(private seller: UserServiceService, private http: HttpClient,) {
    this.baseurl = seller.baseapiurl2
    this.user_web_privacy_policy = this.baseurl+"api/getWebPageBySlug/user_web_privacy_policy"
   }

  ngOnInit(): void {
    this.http.get(this.user_web_privacy_policy).subscribe(res=>{
      this.user_web_privacy_policyRes = res
      if(this.user_web_privacy_policyRes.success){
        this.myRec = this.user_web_privacy_policyRes.data 
        this.termsPage = this.myRec.description
       
      }
    })
  }
}
